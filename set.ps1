notepad 'C:\projects\soundsetup\readme.txt'

$devices = @("2- BEHRINGER UMC 404HD 192k\Device\IN 1-2\Capture",
	     "VB-Audio VoiceMeeter VAIO\Device\VoiceMeeter Input\Render",
	     "VB-Audio VoiceMeeter VAIO\Device\VoiceMeeter Output\Capture"
	    )

$bitdepth = 16
$samplerate = 44100
# $bitdepth = 24
# $samplerate = 48000

foreach ($device in $devices) {
    echo "soundvolumeview /SetDefaultFormat $device $bitdepth $samplerate"
    soundvolumeview /SetDefaultFormat $device $bitdepth $samplerate
}

soundvolumeview /SetDefault "VB-Audio VoiceMeeter VAIO\Device\VoiceMeeter Input\Render" 0 
soundvolumeview /SetVolume "VB-Audio VoiceMeeter VAIO\Device\VoiceMeeter Input\Render" 100

voicemeeter -L ".\voicemeeter.xml"
C:\Users\SacredCircle\AppData\Local\butt\butt
# C:\Users\SacredCircle\AppData\Roaming\Zoom\bin\Zoom
Start-Process "https://us02web.zoom.us/s/5035759001"
start obs64 -workingdirectory "C:\Program Files\obs-studio\bin\64bit" --startvirtualcam
start "C:\Program Files (x86)\Power Automate Desktop\PAD.Console.Host.exe"

Start-Sleep -s 3

$code=@'
using System;
using System.ComponentModel;
using System.Runtime.InteropServices;

static public class Win32{ 
	[DllImport("user32.dll")]
  	[return: MarshalAs(UnmanagedType.Bool)]
  	static public extern bool SetForegroundWindow(IntPtr hWnd);
}
'@
 Add-Type $code 

$handles=(get-process PAD.Console.Host).MainWindowHandle
$handles|%{[win32]::SetForegroundWindow($_)}
