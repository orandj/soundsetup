SACRED CIRCLE DANCE SOUND SETUP
2/19/2022

STREAMING:
1. Plug the Behringer sound card into the laptop using the beige USB cable.
2. Connect the Booth output on the mixer (RCA) to Behringer sound card Inputs 1 and 2 (**FRONT**) using the RCA->stereo 1/4" cable.
3. Plug the Stream Webcam USB cable into the laptop.

4. double click "Sacred Circle Setup Script" on the desktop:
  - it normalizes sound settings, and starts voicemeeter, zoom, butt, and OBS

5. In Voicemeeter's upper left corner, click dropdown and choose the first selection (WDM IN 1-2)

6. In Zoom, click "New Meeting"
  - (if it asks you to log in, use our login, info@sacredcircledance.org, pwd: Tiffany1)

7. In the zoom meeting window:
  - in meeting window, click "join with computer audio"
  - for Video, make sure "OBS virtual camera" is selected
  - click Participants -> ... -> "Mute Participants upon Entry"
  - click Share Screen -> Advanced -> Share Computer Audio
    - in dropdown select "Stereo (High fidelity)"
    - make sure "Share Computer Audio" is selected (blue), and click Share

8. Open "https://sacredcircledance.org/online-streaming-links" on another device (like a phone), and make sure radio player and zoom both have sound
